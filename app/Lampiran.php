<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lampiran extends Model
{
    protected $table = 'lampiran';

    protected $with = ['item'];

    protected $fillable = [
        'produk_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        // 'produk'
    ];

    public function item()
    {
        return $this->hasMany(LampiranItem::class, 'lampiran_id');
    }

    // protected $appends= [ 'tanggal_update' ];

    public function produk()
    {
        return $this->belongsTo(Product::class, 'produk_id');
    }

    // public function getTanggalUpdateAttribute()
    // {
    //     return $this->produk->tanggal_update->format('Y-m-d H:i:s');
    // }
}
