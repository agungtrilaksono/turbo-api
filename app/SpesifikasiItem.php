<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpesifikasiItem extends Model
{
    protected $table = 'spesifikasi_items';
    
    protected $fillable = [
        'label',
        'deskripsi',
        'spesifikasi_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'id',
        'spesifikasi_id'
    ];

    public function spesifikasi()
    {
        return $this->belongsTo(Spesifikasi::class, 'spesifikasi_id');
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
        ->diffForHumans();
    }
}
