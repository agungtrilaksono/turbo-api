<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informasi extends Model
{
    protected $table = 'informasi';

    protected $fillable = [
        'unspsc',
        'id_kategori_produk_lkpp',
        'nama_produk',
        'no_produk_penyedia',
        'id_manufaktur',
        'id_unit_pengukuran_lkpp',
        'deskripsi_singkat',
        'deskripsi_lengkap',
        'kuantitas_stok',
        'produk_aktif',
        'apakah_produk_lokal',
        'berlaku_sampai',
        'url_produk',
        'image_50x50',
        'image_100x100',
        'image_300x300',
        'image_800x800',
        'produk_id',
        'ignored',
        'priority'
    ];

    protected $dates = [
        'berlaku_sampai',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
        // 'produk'
    ];

    // protected $appends= [ 'tanggal_update' ];

    public function produk()
    {
        return $this->belongsTo(Product::class, 'produk_id');
    }

    // public function getTanggalUpdateAttribute()
    // {
    //     return $this->produk->tanggal_update->format('Y-m-d H:i:s');
    // }
}
