<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'produk';

    public $timestamps = false;

    protected $fillable = [
        'tanggal_update',
        'created_at'
    ];
    
    protected $dates = [
        'tanggal_update',
        'created_at'
    ];

    public function spesifikasi()
    {
        return $this->hasOne(Spesifikasi::class, 'produk_id');
    }

    public function informasi()
    {
        return $this->hasOne(Informasi::class, 'produk_id');
    }

    public function harga()
    {
        return $this->hasOne(Harga::class, 'produk_id');
    }

    public function image()
    {
        return $this->hasOne(Image::class, 'produk_id');
    }

    public function lampiran()
    {
        return $this->hasOne(Lampiran::class, 'produk_id');
    }
}
