<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageItem extends Model
{
    protected $table = 'image_items';
    
    protected $fillable = [
        'image_id',
        'image_50x50',
        'image_100x100',
        'image_300x300',
        'deskripsi'        
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'id',
        'image_id',
        'created_at',
        'updated_at'
    ];

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }
}
