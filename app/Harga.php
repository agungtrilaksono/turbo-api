<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Harga extends Model
{
    protected $table = 'harga';

    protected $fillable = [
        'harga_retail',
        'harga_pemerintah',
        'ongkos_kirim',
        'kurs_id',
        'produk_id',
        'updated_at'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'created_at',
    ];

    // protected $appends= [ 'tanggal_update' ];

    public function produk()
    {
        return $this->belongsTo(Product::class, 'produk_id');
    }

    // public function getTanggalUpdateAttribute()
    // {
    //     return $this->produk->tanggal_update->format('Y-m-d H:i:s');
    // }
}
