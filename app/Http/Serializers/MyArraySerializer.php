<?php

namespace App\Http\Serializers;

use League\Fractal\Serializer\ArraySerializer;

class MyArraySerializer extends ArraySerializer
{
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }

    public function item($resourceKey, array $data, $optional = [])
    {
        if ($resourceKey) {
            if ( $optional ) return [$resourceKey[$optional] => $data];
            else return [$resourceKey => $data];
        }
        return $data;
    }

    /**
     * Serialize the meta.
     *
     * @param array $meta
     *
     * @return array
     */
    public function meta(array $meta)
    {
        if (empty($meta)) {
            return [];
        }

        // For foolish reason
        // return ['meta' => $meta];
        return [];
    }
}