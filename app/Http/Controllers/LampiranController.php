<?php

namespace App\Http\Controllers;

use App\Product;
use App\Lampiran;
use App\LampiranItem;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LampiranController extends Controller
{
    public function updateLampiran(Request $request, $id)
    {
        $this->validate($request, [
            'deskripsi' => 'required',
            'file'      => 'required'
        ]);
    
        $now = Carbon::now()->format('Y-m-d H:i:s');
    
        $input = $request->except('secret_key');
    
        $item = LampiranItem::find($id);
        $item->label = $input['deskripsi'];
        $item->deskripsi = $input['file'];
        $item->save();
    
        if ($item)
        {
            $spec = Lampiran::find( $item->lampiran_id);
            $spec->updated_at = $now;
            $spec->save();
    
            $produk = Product::find( $item->lampiran->produk_id );
            $produk->tanggal_update = $now;
            $produk->save();
    
            unset( $item['lampiran'] );
    
            return response()->json([
                'code'    => Response::HTTP_OK,
                'success' => true,
                'message' => 'update lampiran sukses',
                'data'    => $item
            ]);
        }
        else
        {
            return response()->json([
                'code'    => Response::HTTP_BAD_REQUEST ,
                'success' => false,
                'message' => 'update lampiran gagal',
            ]);
        }
    }
}
