<?php

namespace App\Http\Controllers;

use App\Product;
use App\Image;
use App\ImageItem;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ImageController extends Controller
{
    public function updateImage(Request $request, $id)
    {
        $this->validate($request, [
            'image_50x50'   => 'required',
            'image_100x100' => 'required',
            'image_300x300' => 'required'
        ]);

        $now = Carbon::now()->format('Y-m-d H:i:s');

        $input = $request->except('secret_key');

        $item = ImageItem::find($id);
        $item->image_50x50 = $input['image_50x50'];
        $item->image_100x100 = $input['image_100x100'];
        $item->image_300x300 = $input['image_300x300'];
        $item->save();

        // return $item;

        if ($item)
        {
            $spec = Image::find( $item->image_id);
            $spec->updated_at = $now;
            $spec->save();

            $produk = Product::find( $item->image->produk_id );
            $produk->tanggal_update = $now;
            $produk->save();

            unset( $item['image'] );

            return response()->json([
                'code'    => Response::HTTP_OK,
                'success' => true,
                'message' => 'update image sukses',
                'data'    => $item
            ]);
        }
        else
        {
            return response()->json([
                'code'    => Response::HTTP_BAD_REQUEST ,
                'success' => false,
                'message' => 'update image gagal',
            ]);
        }
    }    
}
