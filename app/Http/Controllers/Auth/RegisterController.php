<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function validateUser(Request $request)
    {
        return $this->validate($request, [
            'username'      =>  'required|min:5',
            'email'     =>  'required|email|max:180',
            'password'  =>  'required'
        ]);
    }

    public function register(Request $request)
    {
        $this->validateUser($request);
        
        $data = $request->all();

        $data['password'] = Hash::make($data['password']);

        $register = User::create($data);

        if (!$register) {
            return response()->json([
                'code'      => Response::HTTP_BAD_REQUEST,
                'success'   => false,
                'message'   => 'Failed to Register new user credentials'
            ]);
        }
        return response()->json([
            'code'      => Response::HTTP_OK,
            'success'   => true,
            'message'   => 'successfully register new user credentials'
        ]);
    }
}
