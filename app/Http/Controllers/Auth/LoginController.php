<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function validateLogin(Request $request)
    {
        return $this->validate($request, [
            'email'     => 'required|email|max:180',
            'password'  => 'required'
        ]);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $email = $request->input('email');
        
        $password = $request->input('password');

        $credentials = User::where('email', $email)->first();

        if (!$credentials) {
            return response()->json([
                'code'      => Response::HTTP_BAD_REQUEST,
                'success'   => false,
                'message'   => 'User not registered'
            ]);
        }else {
            if (Hash::check($password, $credentials->password)) {
                $secret_key = sha1(time());
                $create_token = User::where('id', $credentials->id)->update(['secret_key' => $secret_key]);

                if (!$create_token) {
                    return response()->json([
                        'code'      => Response::HTTP_BAD_REQUEST,
                        'success'   => false,
                        'message'   => 'invalid credentials'
                    ]);
                }else {
                    return response()->json([
                        'code'          => Response::HTTP_OK,
                        'success'       => true,
                        'message'       => 'login successfully',
                        'secret_key'    => $secret_key,
                        'data'          => $credentials
                    ]);
                }
            }else {
                return response()->json([
                    'code'      => Response::HTTP_BAD_REQUEST,
                    'success'   => false,
                    'message'   => 'invalid credentials'
                ]);
            }
        }
    }
}
