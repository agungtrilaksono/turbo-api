<?php

namespace App\Http\Controllers;

use App\Product;
use App\Informasi;
use App\Spesifikasi;
use App\SpesifikasiItem;
use App\Image;
use App\ImageItem;
use App\Harga;
use App\Lampiran;
use Illuminate\Http\Request;
use DB;

use Carbon\Carbon;
use Illuminate\Http\Response;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use App\Http\Transformers\ProductTransformer;
use App\Http\Transformers\ProductDetailTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->now = Carbon::now()->format('Y-m-d H:i:s');
    }

    public function index(Request $request)
    {
        $per_page = $request->input('per_page', 1000);

        $now = Carbon::now()->format('Y-m-d');

        $product = new Product;
        if ($request->has('search')) {
            $product = $product->whereHas('informasi', function($query) use ($request) {
                $query->where('nama_produk', 'like', '%'.$request->search.'%')
                      ->orWhere('no_produk_penyedia', 'like', '%'.$request->search.'%');
            });
        }

        if($request->has('order_by')) {
            if ($request->order_by === 'asc') {
                $product = $product->orderBy('created_at', 'asc');
            }elseif ($request->order_by === 'desc') {
                $product = $product->orderBy('created_at', 'desc');
            }
        }

        if($request->has('no_produk_penyedia')) {
            $product = $product->whereHas('informasi', function($query) use ($request) {
                $query->where('no_produk_penyedia', 'like', '%'.$request->no_produk_penyedia.'%');
            });
        }

        if ($request->has('start_date') || $request->has('end_date'))
        {
            if ($request->has('start_date'))
            {
                $product = $product->whereDate('tanggal_update', '>=', $request->start_date);
            }
            
            if ($request->has('end_date'))
            {
                $product = $product->whereDate('tanggal_update', '<=', $request->end_date);
            }
        }

        $product = $product->paginate( $per_page );
        $collection = $product->getCollection();
        
        $resource = new Collection($collection, new ProductTransformer, 'produk');
        $resource->setPaginator(new IlluminatePaginatorAdapter($product));

        $result = $this->buildResourceResponse($resource);

        $last = [
            'total'        => $product->total(),
            'current_page' => $product->currentPage(),
            'per_page'    => (int) $product->perPage(),
            'total_page'   => $product->lastPage(),
        ];

        $last = array_merge($last, $result);

        return $last;
    }

    public function detail($sku)
    {
        $product = Product::whereHas('informasi', function ($query) use ($sku) {
            $query->where('no_produk_penyedia', $sku);
        })->first();
        // return $product;
        if ($product) {
            $resource = new Item($product, new ProductDetailTransformer);
            return $this->buildResourceResponse($resource);
        }
        return response()->json([
            'code'      => Response::HTTP_NOT_FOUND,
            'success'   => false,
            'message'   => 'Data not found'
        ]);
    }

    public function createInformation($product_id, Request $request)
    {
        $informasi = [
            'produk_id' => $product_id,
            'unspsc' => $request->input('informasi.unspsc'),
            'id_kategori_produk_lkpp' => $request->input('informasi.id_kategori_produk_lkpp'),
            'nama_produk' =>  $request->input('informasi.nama_produk'),
            'no_produk_penyedia' =>  $request->input('informasi.no_produk_penyedia'),
            'id_manufaktur' => $request->input('informasi.id_manufaktur'),
            'id_unit_pengukuran_lkpp' => $request->input('informasi.id_unit_pengukuran_lkpp'),
            'deskripsi_singkat' => $request->input('informasi.deskripsi_singkat'),
            'deskripsi_lengkap' => $request->input('informasi.deskripsi_lengkap'),
            'kuantitas_stok' => $request->input('informasi.kuantitas_stok'),
            'produk_aktif' => $request->input('informasi.produk_aktif'),
            'apakah_produk_lokal' => $request->input('informasi.apakah_produk_lokal'),
            'berlaku_sampai' => $request->input('informasi.berlaku_sampai'),
            'url_produk' => $request->input('informasi.url_produk'),
            'image_50x50' => $request->input('informasi.image_50x50'),
            'image_100x100' => $request->input('informasi.image_100x100'),
            'image_300x300' => $request->input('informasi.image_300x300'),
            'image_800x800' => $request->input('informasi.image_800x800'),
            'tanggal_update' => $this->now
        ];
 
        Informasi::create($informasi);
    }

    public function createSpesification($product_id, Request $request)
    {
        $spec = Spesifikasi::create([
            'produk_id' => $product_id
        ]);

        foreach ( $request->spesifikasi as $spesifikasi )
        {
            SpesifikasiItem::create([
                'spesifikasi_id'    => $spec->id,
                'label'             => $spesifikasi['label'],
                'deskripsi'         => $spesifikasi['deskripsi']
            ]);
        }
    }

    public function createImageItem($product_id, Request $request)
    {
        $images = Image::create([
            'produk_id' => $product_id
        ]);

        foreach ($request->image as $image) {
            ImageItem::create([
                'image_id'      => $images->id,
                'deskripsi'     => $image['deskripsi'],
                'image_50x50'   => $image['image_50x50'],
                'image_100x100' => $image['image_100x100'],
                'image_300x300' => $image['image_300x300']
            ]);
        }
    }

    public function createHarga($product_id, Request $request)
    {
        $harga = [
            'produk_id'         => $product_id,
            'harga_retail'      => $request->input('harga.harga_retail'),
            'harga_pemerintah'  => $request->input('harga.harga_pemerintah'),
            'ongkos_kirim'      => $request->input('harga.ongkos_kirim'),
            'kurs_id'           => $request->input('harga.kurs_id')
        ];

        Harga::create($harga);
    }

    public function createLampiranItem($product_id, Request $request)
    {
        $lam = Lampiran::create([
            'produk_id'     => $product_id
        ]);

        foreach ($request->input('lampiran') as $lampiran) {
            LampiranItem::create([
                'lampiran_id'   => $lam->id,
                'deskripsi'     => $lampiran['deskripsi'],
                'file'          => $lampiran['file']
            ]);
        }
    }

    public function store(Request $request)
    {
        // $product = [];
        
        // return $request->input('informasi.unspsc');
        // try {
        //     DB::beginTransaction();
        //     $product = Product::create([
        //         'tanggal_update' => $this->now,
        //         'created_at' => $this->now
        //     ]);
    
        //     if($product) {
        //         $this->createInformation($product->id, $request);
        //         $this->createSpesification($product->id, $request);
        //         $this->createImageItem($product->id, $request);
        //         $this->createHarga($product->id, $request);
        //         $this->createLampiranItem($product->id, $request);
        //     }

        //     DB::commit();
        //     return response($product);
        // } catch (Exception $e) {
        //     DB::rollBack();

        //     return response($e, 400);
        // }

        $data	= $request->all();
		
		$check	= Informasi::where('no_produk_penyedia', $data['informasi']['no_produk_penyedia'])->first();
		
		if ( $check )
		{
			return $this->update($request, $check['no_produk_penyedia']);
		}
		
        $product = Product::create([
            'tanggal_update' => $this->now,
            'created_at' => $this->now
        ]);

        if($product) {
            $this->createInformation($product->id, $request);
            $this->createSpesification($product->id, $request);
            $this->createImageItem($product->id, $request);
            $this->createHarga($product->id, $request);
            $this->createLampiranItem($product->id, $request);
        }

        return response($product);
    }

    public function update(Request $request, $id) 
    {
        $check = Informasi::where(['no_produk_penyedia' => $id])->first();
        if (empty($check)) {
            // create product, spek, informasi, image
            $product = Product::create();
            $this->createInformation($product->id, $request);
            $this->createSpesification($product->id, $request);
            $this->createImageItem($product->id, $request);
            $this->createHarga($product->id, $request);
            $this->createLampiranItem($product->id, $request);
            
        } else {
            //update informasi, spek, image
            $product = Product::where('id', $check->produk_id)->first();
            $this->updateInformation($product->id, $request);
            $this->updateSpesification($product, $request);
            $this->updateImageItem($product, $request);
            $this->updateHarga($product, $request);
        }

        return response()->json([
            'code' => Response::HTTP_OK,
            'message' => 'data updated successfuly',
            'data' => $this->detail($id)
        ]); 
    }

    public function updateInformation($product_id, Request $request)
    {
        $information_attr = [
            'unspsc' => $request->input('informasi.unspsc'),
            'id_kategori_produk_lkpp' => $request->input('informasi.id_kategori_produk_lkpp'),
            'nama_produk' =>  $request->input('informasi.nama_produk'),
            'no_produk_penyedia' =>  $request->input('informasi.no_produk_penyedia'),
            'id_manufaktur' => $request->input('informasi.id_manufaktur'),
            'id_unit_pengukuran_lkpp' => $request->input('informasi.id_unit_pengukuran_lkpp'),
            'deskripsi_singkat' => $request->input('informasi.deskripsi_singkat'),
            'deskripsi_lengkap' => $request->input('informasi.deskripsi_lengkap'),
            'kuantitas_stok' => $request->input('informasi.kuantitas_stok'),
            'produk_aktif' => $request->input('informasi.produk_aktif'),
            'apakah_produk_lokal' => $request->input('informasi.apakah_produk_lokal'),
            'berlaku_sampai' => $request->input('informasi.berlaku_sampai'),
            'url_produk' => $request->input('informasi.url_produk'),
            'image_50x50' => $request->input('informasi.image_50x50'),
            'image_100x100' => $request->input('informasi.image_100x100'),
            'image_300x300' => $request->input('informasi.image_300x300'),
            'image_800x800' => $request->input('informasi.image_800x800'),
            'updated_at' => $this->now
        ];

        Informasi::where('no_produk_penyedia', $request->input('informasi.no_produk_penyedia'))->update($information_attr);
    }

    public function updateSpesification($product, Request $request)
    {
        $i = 0;
        $spesifikasi = Spesifikasi::where('produk_id', $product->id)->first(); 
        foreach ($product->spesifikasi['item'] as $s) {
            $specItem = [ 
                'label' => $request->input('spesifikasi.'.$i.'.label'),
                'deskripsi' => $request->input('spesifikasi.'.$i.'.deskripsi') 
            ];
            SpesifikasiItem::where('id', $s->id)->update($specItem);
            Spesifikasi::where('id', $spesifikasi->id)->update(['updated_at' => $this->now]); 
            $i++;
        } 
    }

    public function updateImageItem($product, Request $request)
    {
        $i = 0;
        $images = Image::where('id', $product->id)->first(); 
        foreach ($product->image['item'] as $image) { 
            $imageItem = [ 
                'deskripsi' => $request->input('image.'.$i.'.deskripsi'), 
                'image_50x50' => $request->input('image.'.$i.'.image_50x50'), 
                'image_100x100' => $request->input('image.'.$i.'.image_100x100'),
                'image_300x300' => $request->input('image.'.$i.'.image_300x300') 
            ]; 
            ImageItem::where('id', $image->id)->update($imageItem); 
            
            Image::where('id', $image->id)->update(['updated_at' => $this->now]);
            $i++;
        }
    }

    public function updateHarga($product, Request $request)
    {
        $harga = [
            'harga_retail' => $request->input('harga.harga_retail'), 
            'harga_pemerintah' => $request->input('harga.harga_pemerintah'),
            'ongkos_kirim' => $request->input('harga.ongkos_kirim'),
            'kurs_id' => $request->input('harga.kurs_id'),
            'updated_at' => $this->now
        ]; 
        Harga::where('produk_id', $product->id)->update($harga); 
    }
}