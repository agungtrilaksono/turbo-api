<?php

namespace App\Http\Controllers;

use App\Product;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HargaController extends Controller
{
    public function updateHarga(Request $request, $id)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');

        $product = Product::find($id);

        $data = $request->except('secret_key');

        $update = $product->harga()->update($data);

        if ($update)
        {

            $produk = Product::find( $id );
            $produk->tanggal_update = $now;
            $produk->save();

            return response()->json([
                'code'    => Response::HTTP_OK,
                'success' => true,
                'message' => 'update spesifikasi sukses',
                'data'    => $data
            ]);
        }
        else
        {
            return response()->json([
                'code'    => Response::HTTP_BAD_REQUEST ,
                'success' => false,
                'message' => 'update spesifikasi gagal',
            ]);
        }
    }
}
