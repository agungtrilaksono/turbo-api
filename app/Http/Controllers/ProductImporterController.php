<?php

namespace App\Http\Controllers;

use App\Product;
use App\Informasi;
use App\Image;
use App\ImageItem;
use App\Lampiran;
use App\LampiranItem;
use App\Spesifikasi;
use App\SpesifikasiItem;
use App\Harga;

class ProductImporterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function importFile(Request $request)
    {
        // if ($request->has('filename')) {
        //     # code...
        // }
    }

    public function exportFile($type)
    {
        $products = $this->product->all()->toArray();

        return \Excel::create('product_export', function ($excel) use ($products) {
            $excel->sheet('product_export', function ($sheet) use ($products) {
                $sheet->fromArray($products);
            });
        })->download($type);
    }
}
