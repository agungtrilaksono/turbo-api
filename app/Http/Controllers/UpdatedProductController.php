<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Http\Response;
use League\Fractal\Resource\Item;
use App\Http\Transformers\ProductDetailTransformer;
use App\Http\Transformers\UpdatedProductTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class UpdatedProductController extends Controller
{
    public function index(Request $request)
    {
        $per_page = $request->input('per_page', 500);

        $now = Carbon::now()->format('Y-m-d');

        $product = new Product;
        $product->whereDate('tanggal_update', '=', $now)->get();
        if ($request->has('search')) {
            $product = $product->whereHas('informasi', function($query) use ($request) {
                $query->where('nama_produk', 'like', '%'.$request->search)
                      ->orWhere('no_produk_penyedia', 'like', '%'.$request->search.'%');
            });
        }

        // $product = $product->orderBy('priority', 'desc');

        if($request->has('order_by')) {
            if ($request->order_by === 'asc') {
                $product = $product->orderBy('created_at', 'asc');
            }elseif ($request->order_by === 'desc') {
                $product = $product->orderBy('created_at', 'desc');
            }
        }
        
        if ($request->has('start_date') || $request->has('end_date'))
        {
            if ($request->has('start_date'))
            {
                $product = $product->whereDate('tanggal_update', '>=', $request->start_date);
            }
            
            if ($request->has('end_date'))
            {
                $product = $product->whereDate('tanggal_update', '<=', $request->end_date);
            }
        }
        else
        {
            $product = $product->whereDate('tanggal_update', '>=', $now);
        }

        $product = $product->paginate( $per_page );
        $collection = $product->getCollection();
        
        $resource = new Collection($collection, new UpdatedProductTransformer, 'produk');
        $resource->setPaginator(new IlluminatePaginatorAdapter($product));
        $result = $this->buildResourceResponse($resource);
        
        $last = [
            'total'        => $product->total(),
            'current_page' => $product->currentPage(),
            'per_page'    => (int) $product->perPage(),
            'total_page'   => $product->lastPage(),
        ];

        $last = array_merge($last, $result);

        return $last;
    }
    
    public function detail($sku)
    {
        $product = Product::whereHas('informasi', function ($query) use ($sku) {
            $query->where('no_produk_penyedia', $sku);
        })->first();
        // return $product;
        if ($product) {
            $resource = new Item($product, new ProductDetailTransformer);
            return $this->buildResourceResponse($resource);
        }
        return response()->json([
            'code'      => Response::HTTP_NOT_FOUND,
            'success'   => false,
            'message'   => 'Data not found'
        ]);
    }
}
