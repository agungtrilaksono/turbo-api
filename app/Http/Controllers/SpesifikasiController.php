<?php

namespace App\Http\Controllers;

use App\Product;
use App\Spesifikasi;
use App\SpesifikasiItem;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SpesifikasiController extends Controller
{
    public function updateSpesifikasi(Request $request, $id)
    {
        $this->validate($request, [
            'label'     => 'required',
            'deskripsi' => 'required'
        ]);

        $now = Carbon::now()->format('Y-m-d H:i:s');

        $input = $request->except('secret_key');

        $item = SpesifikasiItem::find($id);
        $item->label = $input['label'];
        $item->deskripsi = $input['deskripsi'];
        $item->save();

        if ($item)
        {
            $spec = Spesifikasi::find( $item->spesifikasi_id);
            $spec->updated_at = $now;
            $spec->save();

            $produk = Product::find( $item->spesifikasi->produk_id );
            $produk->tanggal_update = $now;
            $produk->save();

            unset( $item['spesifikasi'] );

            return response()->json([
                'code'    => Response::HTTP_OK,
                'success' => true,
                'message' => 'update spesifikasi sukses',
                'data'    => $item
            ]);
        }
        else
        {
            return response()->json([
                'code'    => Response::HTTP_BAD_REQUEST ,
                'success' => false,
                'message' => 'update spesifikasi gagal',
            ]);
        }
    }    
}
