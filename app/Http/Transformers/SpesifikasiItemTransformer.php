<?php

namespace App\Http\Transformers;

use App\SpesifikasiItem;
use League\Fractal\TransformerAbstract;

class SpesifikasiItemTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Spesifikasi  $specs
     * @return array
     */
    public function transform(SpesifikasiItem $specs)
    {
        return [
            'label'     => $specs->label,
            'deskripsi' => $specs->deskripsi
        ];
    }
}
