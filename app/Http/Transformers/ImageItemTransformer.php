<?php

namespace App\Http\Transformers;

use App\ImageItem;
use League\Fractal\TransformerAbstract;

class ImageItemTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Spesifikasi  $specs
     * @return array
     */
    public function transform(ImageItem $imageItem)
    {
        return [
            'deskripsi'       => $imageItem->deskripsi,
            'image_50x50'     => $imageItem->image_50x50,
            'image_100x100'   => $imageItem->image_100x100,
            'image_300x300'   => $imageItem->image_300x300
        ];
    }
}
