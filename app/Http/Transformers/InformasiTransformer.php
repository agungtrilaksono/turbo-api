<?php

namespace App\Http\Transformers;

use App\Informasi;
use League\Fractal\TransformerAbstract;

class InformasiTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Informasi  $info
     * @return array
     */
    public function transform(Informasi $info)
    {
        return [
            'unspsc'                    => $info->unspsc,
            'id_kategori_produk_lkpp'   => $info->id_kategori_produk_lkpp,
            'nama_produk'               => $info->nama_produk,
            'no_produk_penyedia'        => $info->no_produk_penyedia,
            'id_manufaktur'             => $info->id_manufaktur,
            'id_unit_pengukuran_lkpp'   => $info->id_unit_pengukuran_lkpp,
            'deskripsi_singkat'         => $info->deskripsi_singkat,
            'deskripsi_lengkap'         => $info->deskripsi_lengkap,
            'kuantitas_stok'            => $info->kuantitas_stok,
            'produk_aktif'              => $info->produk_aktif,
            'apakah_produk_lokal'       => $info->apakah_produk_lokal,
            'berlaku_sampai'            => $info->berlaku_sampai->format('Y-m-d'),
            'url_produk'                => $info->url_produk,
            'image_50x50'               => $info->image_50x50,
            'image_100x100'             => $info->image_100x100,
            'image_300x300'             => $info->image_300x300,
            'image_800x800'             => $info->image_800x800,
            'tanggal_update'            => $info->updated_at->setTimeZone('+7')->toDateTimeString()
        ];
    }
}
