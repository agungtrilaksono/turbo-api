<?php

namespace App\Http\Transformers;

use App\Lampiran;
use League\Fractal\TransformerAbstract;

class LampiranTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Lampiran 
     * @return array
     */
    public function transform(Lampiran $lampiran)
    {
        return [
            'item'           => $lampiran->item,
            'tanggal_update' => $lampiran->updated_at->setTimeZone('+7')->toDateTimeString()
        ];
    }
}
