<?php

namespace App\Http\Transformers;

use App\Harga;
use League\Fractal\TransformerAbstract;

class HargaTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Product  $manufacture
     * @return array
     */
    public function transform(Harga $harga)
    {
        return [
            'harga_retail'      => $harga->harga_retail,
            'harga_pemerintah'  => $harga->harga_pemerintah,
            'ongkos_kirim'      => $harga->ongkos_kirim,
            'kurs_id'           => $harga->kurs_id,
            'tanggal_update'    => $harga->updated_at->setTimeZone('+7')->toDateTimeString(),
        ];
    }
}
