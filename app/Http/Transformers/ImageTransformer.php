<?php

namespace App\Http\Transformers;

use App\Image;
use League\Fractal\TransformerAbstract;

class ImageTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Image  $spec
     * @return array
     */
    public function transform(Image $image)
    {
        return [
            'item'              => $image->item,
            'tanggal_update'    => $image->updated_at->setTimeZone('+7')->toDateTimeString()
        ];
    }
}
