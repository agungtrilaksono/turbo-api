<?php

namespace App\Http\Transformers;

use App\LampiranItem;
use League\Fractal\TransformerAbstract;

class LampiranItemTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\LampiranItem  $specs
     * @return array
     */
    public function transform(LampiranItem $lampiranItem)
    {
        return [
            'deskripsi'  => $lampiranItem->deskripsi,
            'file'       => $lampiranItem->file,
        ];
    }
}
