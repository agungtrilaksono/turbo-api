<?php

namespace App\Http\Transformers;

use App\Spesifikasi;
use League\Fractal\TransformerAbstract;

class SpesifikasiTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Spesifikasi  $spec
     * @return array
     */
    public function transform(Spesifikasi $spec)
    {
        return [
            'item'           => $spec->item,
            'tanggal_update' => $spec->updated_at->setTimeZone('+7')->toDateTimeString(),
        ];
    }
}
