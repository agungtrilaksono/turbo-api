<?php

namespace App\Http\Transformers;

use App\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'informasi',
        'spesifikasi',
        'image',
        'harga',
        'lampiran'
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @param  \App\Product  $manufacture
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            // 'tanggal_update' => $product->tanggal_update->toDateTimeString(),
        ];
    }

    /**
     * Include Manufactures
     *
     * @return League\Fractal\ItemResource
     */
    public function includeInformasi(Product $product)
    {
        $info = $product->informasi;

        return $this->item($info, new InformasiTransformer);
    }

    /**
     * Include Manufactures
     *
     * @return League\Fractal\ItemResource
     */
    public function includeSpesifikasi(Product $product)
    {
        $spec = $product->spesifikasi;

        return $this->item($spec, new SpesifikasiTransformer);
    }

    /**
     * Include Manufactures
     *
     * @return League\Fractal\ItemResource
     */
    public function includeImage(Product $product)
    {
        $image = $product->image;

        return $this->item($image, new ImageTransformer);
    }
    
    /**
     * Include Manufactures
     *
     * @return League\Fractal\ItemResource
     */
    public function includeHarga(Product $product)
    {
        $harga = $product->harga;

        return $this->item($harga, new HargaTransformer);
    }

    /**
     * Include Manufactures
     *
     * @return League\Fractal\ItemResource
     */
    public function includeLampiran(Product $product)
    {
        $lampiran = $product->lampiran;

        return $this->item($lampiran, new LampiranTransformer);
    }
}
