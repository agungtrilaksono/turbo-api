<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->auth->guard($guard)->guest()) {
            // return response('Unauthorized.', 401);
            if ($request->has('secret_key')) {
                $token = $request->input('secret_key');
                $check_token = User::where('secret_key', $token)->first();
                if ($check_token == null) {
                    return response()->json([
                        'status'    => $res['status'] = 401,
                        'success'   => $res['success'] = false,
                        'message'   => $res['message'] = 'unauthenticated !!',
                    ]);
                }
            }else {
                return response()->json([
                    'status'    => $res['status'] = 401,
                    'success'   => $res['success'] = false,
                    'message'   =>$res['message'] = 'login please !!',
                ]);
            }
        }

        return $next($request);
    }
}
