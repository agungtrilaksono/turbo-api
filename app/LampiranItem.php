<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LampiranItem extends Model
{
    protected $table = 'lampiran_items';

    protected $fillable = [
        'deskripsi',
        'file',
        'lampiran_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'id',
        'lampiran_id'
    ];
}
