<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lampiran_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deskripsi');
            $table->string('file');

            $table->unsignedInteger('lampiran_id');
            $table->foreign('lampiran_id')->references('id')->on('lampiran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lampiran_item');
    }
}
