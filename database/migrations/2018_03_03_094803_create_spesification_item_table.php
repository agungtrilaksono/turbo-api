<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpesificationItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spesifikasi_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('deskripsi');

            $table->unsignedInteger('spesifikasi_id');
            $table->foreign('spesifikasi_id')->references('id')->on('spesifikasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spesifikasi_item');
    }
}
