<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informasi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unspsc');
            $table->string('nama_produk');
            $table->string('no_produk_penyedia');
            $table->unsignedInteger('id_manufaktur');
            $table->unsignedInteger('id_unit_pengukuran_lkpp');
            $table->string('deskripsi_singkat');
            $table->text('deskripsi_lengkap');
            $table->unsignedInteger('kuantitas_stok');
            $table->boolean('produk_aktif')->default(1);
            $table->boolean('apakah_produk_lokal')->default(0);
            $table->date('berlaku_sampai');
            $table->string('url_produk');
            $table->string('image_50x50');
            $table->string('image_100x100');
            $table->string('image_300x300');
            $table->string('image_800x800');

            $table->unsignedInteger('produk_id');
            
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->boolean('ignored')->default(false);
            $table->integer('priority')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informasi');
    }
}
