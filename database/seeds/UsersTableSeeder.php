<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $user = \App\User::create([
            'username' => 'agungtri',
            'email'    => 'agungtrilaksono123@gmail.com',
            'password' => Hash::make('agungtri'),
            'secret_key' => '4510deb00627a4dabb800d0f485af867f4e836e7'
        ]);
    }
}
