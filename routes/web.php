<?php

use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('test', function() use ($router){
    // return Product::with('informasi', 'spesifikasi', 'harga', 'lampiran', 'image')->first();
    return Hash::make('lkpp899');
});

$router->get('/key', function() use ($router) {
    return sha1(time());
});

$router->get('export-file/{type}', 'ProductImporterController@exportFile');

$router->get('/', function () use ($router) {
    return response()->json([
        'code' => 200,
        'success' => true,
        'message' => 'Turbo Api version 1.0'
    ]);
});

$router->post('/login', ['uses' => 'Auth\LoginController@login', 'as' => 'user.login']);
$router->post('/register', ['uses' => 'Auth\RegisterController@register', 'as' => 'user.register']);

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('produk', ['uses' => 'ProductController@index', 'as' => 'product.index']);
    $router->get('all_produk', ['uses' => 'ProductController@index', 'as' => 'product_all.index']);
    $router->get('updated_produk', ['uses' => 'UpdatedProductController@index', 'as' => 'updatedProduct.index']);
    $router->get('produk/{sku}', ['uses' => 'ProductController@detail', 'as' => 'product.detail']);
    $router->get('updated_produk/{sku}', ['uses' => 'UpdatedProductController@detail', 'as' => 'updatedProduct.detail']);
    $router->put('produk/update_harga/{id}', ['uses' => 'HargaController@updateHarga', 'as' => 'harga.update']);
    $router->put('produk/update_info/{id}', ['uses' => 'InformasiController@updateInformasi', 'as' => 'informasi.update']);
    $router->put('produk/update_spec/{id}', ['uses' => 'SpesifikasiController@updateSpesifikasi', 'as' => 'spesifikasi.update']);
    $router->put('produk/update_image/{id}', ['uses' => 'ImageController@updateImage', 'as' => 'image.update']);
    $router->put('produk/update_lampiran/{id}', ['uses' => 'LampiranController@updateLampiran', 'as' => 'lampiran.update']);
    $router->post('produk', ['uses' => 'ProductController@store']);
    $router->put('produk/update/{id}', ['uses' => 'ProductController@update', 'as' => 'produk.update']);
});